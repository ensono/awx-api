from REST import REST

class AWX_API(object):
    """
    This class implements the AWX REST API.
    
    The naming convetion used for the methods converts the URI to a name by
    replacing / with _ and then appending the type of call (e.g. get).
    """

    def __init__(self, input_base_url = '', input_personal_access_token = ''):
        self._REST = REST(input_base_url, input_personal_access_token)

    def api_get(self):
        response = self._REST.get('/api/')
        return(response)

    def api_v2_get(self):
        response = self._REST.get('/api/v2/')
        return(response)

    def api_v2_me_get(self):
        response = self._REST.get('/api/v2/me/')
        return(response)

    def api_v2_users_get(self, id = ''):
        uri = "/api/v2/users/"
        if (id):
            uri += str(id) + "/"
        response = self._REST.get(uri)
        return(response)

    def api_v2_organizations_delete(self, id):
        uri = "/api/v2/organizations/" + str(id) + "/"
        response = self._REST.delete(uri)
        return(response)

    def api_v2_organizations_get(self):
        response = self._REST.get('/api/v2/organizations/')
        return(response)

    def api_v2_organizations_post(self, name, description=''):
        headers = {
            'Content-Type': 'application/json'
        }
        data = {
            "description": description,
            "name": name
        }
        response = self._REST.post('/api/v2/organizations/', headers, data)
        return(response)

    def api_v2_organizations_put(self, id, description=''):
        headers = {
            'Content-Type': 'application/json'
        }
        uri = "/api/v2/organizations/" + str(id) + "/"
        response = self._REST.get(uri)
        updated_data = response.json()
        updated_data["description"] = description
        response = self._REST.put(uri, headers, updated_data)
        return(response)

    
    def api_v2_projects_get(self):
        response = self._REST.get('/api/v2/projects')
        return(response)
    
    def api_v2_job_templates_get(self):
        response = self._REST.get('/api/v2/job_templates')
        return(response)
    
    def api_v2_jobs_get(self):
        response = self._REST.get('/api/v2/jobs')
        return(response)
    
    def api_v2_inventories_get(self):
        response = self._REST.get('/api/v2/inventories')
        return(response)



    def __str__(self):
        output = ""
        output += "AWX_API  Object:\n"
        output += "----------------\n"
        output += str(self._REST)
        output += "\n"
        return(output)

