#! /usr/bin/env python

# standard modules
import argparse
import getpass
import sys

# custom modules
from AWX import AWX
import RestExceptions

def main():
    # get any parameters
    args = usage()

    base_url = 'http://192.168.49.2:31944'
    personal_access_token = getpass.getpass(prompt='Personal Access Token: ')

    try:
        # instantiate an AWX object
        awx = AWX(base_url, personal_access_token)

        # ensure the Lafond organization does not exist
        print("delete the Lafond organization")
        awx.delete_organization('Lafond')

        # create the Lafond organization
        print("create the Lafond organization")
        awx.create_organization('Lafond', 'Test Organization')

        # change the description of the Lafond organization
        print("change the description of the Lafond organization")
        awx.change_organization_description('Lafond', 'Lafond Organization')

        # disply the awx object
        print(awx)

    except RestExceptions.RestError as e:
        print(e)
        sys.exit(1)

    sys.exit(0)

def usage():
    parser = argparse.ArgumentParser(description="please define")
    parser.add_argument("-p", "--parameter", dest="parameter", \
        default="", help="parameter")
    args = parser.parse_args()

    return (args)

if __name__ == '__main__':
    main()

