from AWX_API import AWX_API

class AWX(object):

    def __init__(self, input_base_url = '', input_personal_access_token = ''):
        self._AWX_API = AWX_API(input_base_url, input_personal_access_token)

    def change_organization_description(self, name, new_description):
        id = self.get_organization_id(name)
        if (id != -1):
            response = self._AWX_API.api_v2_organizations_put(id, new_description)

    def create_organization(self, name, description):
        id = self.get_organization_id(name)
        if (id == -1):
            response = self._AWX_API.api_v2_organizations_post(name, description)

    def delete_organization(self, name):
        id = self.get_organization_id(name)
        if (id != -1):
            response = self._AWX_API.api_v2_organizations_delete(id)

    def get_organization_id(self, name):
        response = self._AWX_API.api_v2_organizations_get()
        organization_id = -1
        for organization in response.json()['results']:
            if (organization['name'] == name):
                organization_id = organization['id']
                break
        return(organization_id)

    def __str__(self):
        output = ""
        output += "AWX Object:\n"
        output += "-----------\n"
        output += str(self._AWX_API)
        output += "\n"
        return(output)

